/*
 * ============================================================================
 *
 *       Filename:  philosopher.cc
 *
 *    Description:  Implementação de uma solução completa para o problema
 *                  do Jantar do Filósofos
 *
 *        Version:  1.0
 *        Created:  10/02/2018 11:3N:N4 PM
 *       Compiler:  gcc
 *
 *         Author:  Gustavo Pantuza (gustavopantuza@gmail.com)
 *   Organization:  Comunidade Brasileira de Ciência da Computação
 *
 * ============================================================================
 */


#include <iostream>
#include <thread>
#include <vector>
#include <chrono>
#include <mutex>
#include <chrono>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/times.h>

using namespace std;
using namespace std::chrono;
using std::thread;
using std::cout;
using std::endl;
using std::vector;
using std::this_thread::sleep_for;
using std::rand;
using std::chrono::milliseconds;
using std::mutex;
using std::lock;
using std::lock_guard;
using std::adopt_lock;

#define N 5

//vetores de comparacao
unsigned long long int quantidadeComendo[N];
unsigned long long int quantidadePensando[N];

high_resolution_clock::time_point tempoInicial;
high_resolution_clock::time_point tempoFinal;
duration<double> tempoAtual;
duration<double> DURACAO;

struct rusage r_usage;
long double a[4],b[4],loadavg;
FILE* CPU;

string arquivosNomes[5] = {"memoria1.txt","memoria2.txt","memoria3.txt","memoria4.txt","memoria5.txt"};
int posicaoArquivo = 0;

long double momentoCPU(){
    CPU = fopen("/proc/self/status","r");
    fscanf(CPU,"%*s %Lf %Lf %lF %lF", &a[0], &a[1], &a[2], &a[3]);
    fclose(CPU);
    sleep(1);
    CPU = fopen("/proc/stat","r");
    fscanf(CPU,"%*s %Lf %Lf %lF %lF", &b[0], &b[1], &b[2], &b[3]);
    fclose(CPU);
    loadavg = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0] + a[1] + a[2] + a[3]));
    loadavg *= 10;
    printf("A atual utilizacao da CPU e: %Lf%%\n",loadavg);
    return loadavg;
}


void escreverMemoria(){
    FILE* arquivo;
    long double porcetagem;
    if ((arquivo = fopen(arquivosNomes[posicaoArquivo].c_str(),"w")) == NULL){
            
            puts ("Arquivo nao pode ser aberto...");
            
            exit (1);
    }

    double aux = tempoAtual.count();
    porcetagem = momentoCPU();
    getrusage(RUSAGE_SELF,&r_usage);
    fprintf(arquivo,"Tempo atual de execucao: %f segundos\n",aux);
    fprintf(arquivo,"A atual utilizacao da CPU e: %Lf%%\n", porcetagem);
    fprintf(arquivo,"Memoria:\n");
    fprintf(arquivo,"Tamanho maximo do conjuto de residentes: %ld KB\n", r_usage.ru_maxrss);
    fprintf(arquivo,"Tamanho da memoria compartilhada: %ld KB\n", r_usage.ru_maxrss);
    fprintf(arquivo,"Tamanho da memoria nao compartilhada: %ld KB\n", r_usage.ru_ixrss);
    fprintf(arquivo,"Opcoes de contexto voluntario: %ld \n", r_usage.ru_nvcsw);
    fprintf(arquivo,"Opcoes de contexto involuntario: %ld \n", r_usage.ru_nivcsw); 
    fclose(arquivo);

    cout << "Ultimo arquivo escrito:" << posicaoArquivo + 1 << endl;

  if(posicaoArquivo == 4){
    posicaoArquivo = 0;
  }else{
    posicaoArquivo++;
  }
}


/**
 * Class that implements a dinner Table with all the forks resources
 */
class Table {
  public:

    /* The shared resource that each Philosopher will concur */
    vector<mutex> forks;

    Table (): forks(N)
    {
    }
};



/**
 * Class that implements the existence of a Philosopher that is
 * to think and eat
 */
class Philosopher {

  public:
    Philosopher (const int id, Table& table)
    :philosopher_id(id),
     table(table)
    {
    }

    /*
     * Spawns the parallel execution of a Philosopher that shares the same
     * table with other Philosophers
     */
    thread* Spawn ()
    {
      return new thread([=] { exist(); });
    }

  private:
    /* The Philosopher identifier */
    int philosopher_id;

    /* A reference to the shared table instance */
    Table& table;


    void exist ()
    {
        while(tempoAtual <  DURACAO){
            tempoFinal = high_resolution_clock::now();
            tempoAtual = duration_cast<duration<double> >(tempoFinal - tempoInicial);
            think();
            eat();
            if(7 == rand() % 10){
              escreverMemoria();
            }
      }
    }

    /**
     * The randomized time that each Philosopher spends thinking
     */
    void think ()
    {
      cout << "Filosofo " << philosopher_id << " pensando.." << endl;
      quantidadePensando[philosopher_id]++;
      sleep_for(milliseconds(rand() % (N * 1000)));
    }

    /**
     * The eating process of each Philosopher instance. First of all, a
     * Philosopher tries to lock both forks that he needs to eat. There
     * are N forks. If one fork is acquired by any other Philosopher, the
     * thread will wait until the resource/fork is released.
     *
     * When both forks are acquired by the current thread execution, it time
     * to the Philosopher to eat. At the end of this process both forks are
     * released.
     */
    void eat ()
    {
      int left = philosopher_id;
      int right = (philosopher_id + 1) % table.forks.size();

      cout << "Filosofo " << philosopher_id << " pegando garfo.." << endl;

      /* Locks boths mutexes without any deadlock */
      lock(table.forks[left], table.forks[right]);

      /* At the end of this scope mutexes will be released */
      lock_guard<mutex> left_fork(table.forks[left], adopt_lock);
      lock_guard<mutex> right_fork(table.forks[right], adopt_lock);

      cout << "Filosofo " << philosopher_id << " comendo.." << endl;
      quantidadeComendo[philosopher_id]++;
      sleep_for(milliseconds(rand() % (N * 1000)));

      cout << "Filosofo " << philosopher_id << " soltando garfo.." << endl;
    }
};

void imprimirResultados(string nome){
    FILE* arquivo;
    long double porcetagem;
    if ((arquivo = fopen(nome.c_str(),"w")) == NULL){
            arquivo = fopen(nome.c_str(),"w");
            puts ("Arquivo nao pode ser aberto...");
            for(int i = 0; i < N; i++){
                printf("Quantidade que o filosofo %d pensou: %llu \n", i + 1,quantidadePensando[i]);
                printf("Quantidade que o filosofo %d comeu: %llu \n", i + 1,quantidadeComendo[i]);
                printf("\n");
            }
            exit (1);
    }

    for(int i = 0; i < N; i++){
      fprintf(arquivo,"Quantidade que o filosofo %d pensou: %llu \n", i + 1,quantidadePensando[i]);
      fprintf(arquivo,"Quantidade que o filosofo %d comeu: %llu \n", i + 1,quantidadeComendo[i]);
      fprintf(arquivo,"\n");
    }
    double aux = tempoAtual.count();
    porcetagem = momentoCPU();
    getrusage(RUSAGE_SELF,&r_usage);
    fprintf(arquivo,"Tempo total de execucao: %f segundos\n",aux);
    fprintf(arquivo,"A atual utilizacao da CPU e: %Lf%%\n", porcetagem);
    fprintf(arquivo,"Memoria:\n");
    fprintf(arquivo,"Tamanho maximo do conjuto de residentes: %ld KB\n", r_usage.ru_maxrss);
    fprintf(arquivo,"Tamanho da memoria compartilhada: %ld KB\n", r_usage.ru_maxrss);
    fprintf(arquivo,"Tamanho da memoria nao compartilhada: %ld KB\n", r_usage.ru_ixrss);
    fprintf(arquivo,"Opcoes de contexto voluntario: %ld \n", r_usage.ru_nvcsw);
    fprintf(arquivo,"Opcoes de contexto involuntario: %ld \n", r_usage.ru_nivcsw); 
    fclose(arquivo);
}

int
main (int argc, char* argv[])
{
    momentoCPU();
    double tsegundos;
    cout << "Digite o tempo que o programa deve executar em segundos:";
    cin >> tsegundos;
    duration<double> auxliar(tsegundos);
    DURACAO = auxliar;
    string nome;
    cout << "Digite o nome do arquivo que vai salvar: ";
    cin >> nome;
    for(int i = 0; i < N; i++){
        quantidadeComendo[i] = 0;
        quantidadePensando[i] = 0;
    }
      tempoInicial = high_resolution_clock::now();
  cout << "O jantar dos filosofos" << endl << endl;
  Table table;

  vector<thread *> threads(N);

  for(int i=0; i < N; i++) {

    Philosopher* philosopher = new Philosopher(i, table);
    threads[i] = philosopher->Spawn();
  }

  for(int i=0; i < N; i++) {

    threads[i]->join();
  }
  
  cout << "Ultimo arquivo de memoria escrito:" << posicaoArquivo << endl;
  imprimirResultados(nome);
  
  return EXIT_SUCCESS;
}
